from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'index.html')

def story4(request):
    return render(request, 'story4.html')

def hub(request):
    return render(request, 'hub.html')