from django.urls import path
from . import views

app_name = 'application'

urlpatterns = [
    path('', views.hub, name='hub'),
    path('story1/', views.index, name='index'),
    path('story3/', views.story4, name='story4'),
]